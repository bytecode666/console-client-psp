package readerthread;

import chatclient.ChatClient;
import chatclient.Client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReaderRunnable implements Runnable {

    private Client client;
    private boolean waitingToServer;
    private boolean emptyChatrooms;

    public ReaderRunnable(Client client) {

        this.client = client;
        this.waitingToServer = false;
    }

    /**
     * Getter of attribute waitingToServer. It is synchronized because the
     * client threads will get the value of this attribute.
     */
    public synchronized boolean isWaitingToServer() {
        return waitingToServer;
    }

    /**
     * Sets the value true/false if the client is waiting for server response.
     * When the reader gets the server response, this thread will be awaken.
     *
     * @param waitingToServer
     */
    public synchronized void setWaitingToServer(boolean waitingToServer) {
        this.waitingToServer = waitingToServer;
        notifyAll();
    }

    /**
     * Sets the value true/false if there is no chatrooms created in server. It
     * is synchronized because the client threads will be changing the value of
     * this attribute.
     *
     * @param port
     */
    public synchronized void setIsChatroomEmpty(boolean emptyChatrooms) {
        this.emptyChatrooms = emptyChatrooms;
    }

    /**
     * Getter of attribute isEmptyChatrooms. It is synchronized because the
     * client threads will get the value of this attribute.
     */
    public synchronized boolean isEmptyChatrooms() {
        return emptyChatrooms;
    }

    @Override
    public void run() {

        String message = "";
        JSONParser parser = new JSONParser();

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(client.getClientSocket().getInputStream()));

            while (!client.getClientSocket().isClosed()) {

                try {

                    message = br.readLine();

                    // The message is sended in JSON format, that is the reason I ask for the character "{".
                    if (message != null && !message.isEmpty() && message.contains("{")) {

                        JSONObject jsonGeneralObject = (JSONObject) parser.parse(new String(message.getBytes(), Charset.forName("UTF-8")));

                        /* Gets the chatrooms that are in server and shows them if there any, 
                        otherwhise I show the message that is sended by server. */
                        if (jsonGeneralObject.containsKey("chatrooms")) {

                            JSONArray array = (JSONArray) jsonGeneralObject.get("chatrooms");

                            if (jsonGeneralObject.containsKey("emptyChatrooms")) {

                                System.out.println(jsonGeneralObject.get("emptyChatrooms"));

                                // Tells to the client the server doesn't has any chatroom created.
                                setIsChatroomEmpty(true);

                            } else {

                                // Tells to the client the server has chatrooms created.
                                setIsChatroomEmpty(false);

                                for (Object object : array) {
                                    JSONObject jsonObjChatroom = (JSONObject) object;
                                    System.out.println(jsonObjChatroom.get("availablechats")
                                            + " " + jsonObjChatroom.get("name") + " " + jsonObjChatroom.get("thematic")
                                            + " " + jsonObjChatroom.get("numbofpeople"));
                                }
                            }

                        } else if (jsonGeneralObject.containsKey("clientAdded")) {

                            JSONObject clientAdded = (JSONObject) jsonGeneralObject.get("clientAdded");

                            if (clientAdded.get("isClientAdded") != null && clientAdded.get("isClientAdded").equals(true)) {

                                // Tells the client it is chatting in the chosen chatroom.
                                client.setIsInChat(true);

                                // Shows the initial messages sended by the server
                                System.out.println(System.lineSeparator() + clientAdded.get("messages").toString() + System.lineSeparator());
                                ChatClient.showCommands();

                            } else {

                                /*In case the client has chosen an inexistent chatroom, or the client cannot be added, 
                                tells to the client it is not chatting.*/
                                client.setIsInChat(false);
                                
                                // Shows the messages sended by the server(ex: wrong position chosen of a chatroom, no chatrooms created..)
                                System.out.println(System.lineSeparator() + clientAdded.get("messages").toString());
                            }

                        } else {

                            // Gets and show the messages sended by the client to the chatroom.
                            System.out.println(System.lineSeparator() + System.lineSeparator() + System.lineSeparator());
                            JSONArray array = (JSONArray) jsonGeneralObject.get("messages");
                            
                            for (Object object : array) {
                                JSONObject jsonObjectMessage = (JSONObject) object;
                                ChatClient.showMessagesInChat(jsonObjectMessage);
                            }
                        }

                    } else {
                        
                        // Shows any other message sended by client.
                        System.out.println(message);
                    }

                } catch (java.net.SocketException ex) {
                } catch (ParseException ex) {
                    System.err.println("Error while parsing JSON in " + ex.getMessage());
                }

                // In this point, there is no need to wait for any response from the server.
                this.setWaitingToServer(false);
            }

        } catch (IOException ex) {
            System.err.println("Error while reading messages: " + ex.getMessage());
        }
    }
}