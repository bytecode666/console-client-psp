package chatclient;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import org.json.simple.JSONObject;
import readerthread.ReaderRunnable;

/**
 *
 * @author silvia
 */
public class ChatClient {

    private static final int DEFAULT_PORT = 4444;
    private static final String DEFAULT_MACHINE = "localhost";
    public static final String EXIT_KEY = "/quit";
    public static final String HELP_KEY = "/help";
    public static final String BACK_KEY = "/back";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final HashMap<String, String> COLOR_MESSAGES = new HashMap<>();

    private static final int OPTION_CHAT = 1;
    private static final int OPTION_SHOW_CHATROOMS = 2;
    public static final int OPTION_CREATE_CHATROOM = 3;
    private static final int OPTION_JOIN_CHAT = 4;
    private static final int OPTION_HELP = 5;
    private static final int OPTION_EXIT = 6;
    private static final int OPTION_LEAVE_CHATROOM = 7;

    public static void main(String[] args) throws InterruptedException {

        int port = DEFAULT_PORT;
        String machine = DEFAULT_MACHINE;

        try {

            if (args.length > 0 && !args[0].isEmpty() && !args[1].isEmpty()) {

                port = Integer.parseInt(args[0]);
                machine = args[1];
            }

            putColorsInHashmap("pink", "\u001B[31m");
            putColorsInHashmap("red", "\u001B[34m");
            putColorsInHashmap("purple", "\u001B[35m");

            System.out.println("¡WELCOME TO MY CONSOLE CHAT APLLICATION!");
            System.out.println("----------------------------------------");

            System.out.println(System.lineSeparator() + "CREATING CLIENT SOCKET");
            System.out.println("TRYING TO CONNECT TO SERVER.." + "[" + machine + "," + port + "]");
            InetSocketAddress socketAddr = new InetSocketAddress(machine, port);

            String nickname = "";

            while (nickname.isEmpty()) {

                nickname = checkStringEmpty("Set your nickname: ");
            }

            Client client = createNewClient(nickname);
            client.getClientSocket().connect(socketAddr);

            System.out.println(System.lineSeparator() + "CONNECTION TO SERVER STABLISHED ON PORT "
                    + client.getClientSocket().getLocalPort());

            ReaderRunnable reader = readMessagesFromServer(client);
            mainLoop(client, reader);

        } catch (IOException ex) {
            System.out.println(ex.getMessage());

        } catch (ArrayIndexOutOfBoundsException ex) {
            System.err.println("Error, you must pass by args <port> <machine address>");

        } catch (NumberFormatException ex) {
            System.err.println("The port must be numeric! " + ex.getMessage());
        }
    }

    /**
     * Controls the loop of the main menu of the program.
     * @param client
     * @param reader
     * @throws IOException
     */
    public static void mainLoop(Client client, ReaderRunnable reader) throws IOException {

        Scanner keyboard = new Scanner(System.in);
        int optionChosen = 0;
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(client.getClientSocket().getOutputStream()));

        // Shows the available chatrooms in the server.
        showChatrooms(bw);

        // This will make the reader runnable wait 1 second for the server response.
        reader.setWaitingToServer(true);
        synchronizeCurrentThread(reader);

        do {

            String message = "";
            JSONObject jsonOptionChosen = new JSONObject();

            if (client.getIsInChat()) {

                // Request to the server to enter in the chat mode.
                optionChosen = OPTION_CHAT;

                while (message.isEmpty()) {

                    message = checkStringEmpty("");
                }

                executeOptions(message, client, jsonOptionChosen, bw);

            } else {

                // If there are chatrooms, when the client is in, the main menu is displayed.
                if (!reader.isEmptyChatrooms()) {

                    printMainMenu();

                    while (!checkValidOption(keyboard)) {

                        System.out.println(System.lineSeparator() + "The option must be a number, please, try again");
                        keyboard.nextLine();
                        printMainMenu();
                    }

                    optionChosen = keyboard.nextInt();

                } else {

                    // If it is the first client, it request to the server to create a chatroom.
                    optionChosen = OPTION_CREATE_CHATROOM;
                }

                switch (optionChosen) {

                    case OPTION_CREATE_CHATROOM:
                        createChatroom(client, jsonOptionChosen);
                        break;

                    case OPTION_JOIN_CHAT:
                        // Waits 1 second for the server response.
                        if (joinChatroom(jsonOptionChosen, keyboard, bw, reader)) {
                            reader.setWaitingToServer(true);
                        }
                        break;

                    case OPTION_EXIT:
                        System.out.println(System.lineSeparator() + "The program is closed..");
                        client.getClientSocket().close();
                        break;

                    default:
                        System.out.println(System.lineSeparator() + "Incorrect option");
                }
            }

            jsonOptionChosen.put("option", optionChosen);
            writeInBuffer(jsonOptionChosen.toJSONString(), bw);

            /* Synchronizes the current thread if the option is join to chat, 
            this will make the program work well if there are many client threads executing simultaneously*/
            if (optionChosen == OPTION_JOIN_CHAT) {

                synchronizeCurrentThread(reader);
            }

            /* This "breaks" a bit the loop, and avoids to ask again to the user 
            to create a chatroom if it returns to main menu.*/
            reader.setIsChatroomEmpty(false);

        } while (optionChosen != OPTION_EXIT);
    }

    /**
     * Synchronizes the current thread and the reader thread will wait to the server 1 second
     * while isWaitingToServer is true.
     * @param reader
     */
    private static void synchronizeCurrentThread(ReaderRunnable reader) {

        synchronized (Thread.currentThread()) {
            while (reader.isWaitingToServer()) {
                try {
                    Thread.currentThread().wait(1000);
                } catch (InterruptedException ex) {
                    System.err.println("Interrupted execution in thread "
                            + Thread.currentThread().getName() + " because of: " + ex.getMessage());
                }
            }
        }
    }

    public static void showMessagesInChat(JSONObject jsonObjectMessage) {

        System.out.println(COLOR_MESSAGES.get((String) jsonObjectMessage.get("color"))
                + jsonObjectMessage.get("nickname")
                + ANSI_RESET + ": "
                + jsonObjectMessage.get("message") + " "
                + jsonObjectMessage.get("hour"));
    }

    public static void showCommands() {

        System.out.println(System.lineSeparator() + "COMMANDS" + System.lineSeparator()
                + EXIT_KEY + " or " + EXIT_KEY.toUpperCase()
                + " to close the program while chatting " + System.lineSeparator()
                + HELP_KEY + " or " + HELP_KEY.toUpperCase() + " to see the commands"
                + System.lineSeparator() + BACK_KEY + " or " + BACK_KEY.toUpperCase() + " to return to main menu");
    }

    /**
     * Requests to the server show the available chatrooms.
     *
     * @param bw
     * @throws IOException
     */
    public static void showChatrooms(BufferedWriter bw) throws IOException {

        JSONObject jsonShowChat = new JSONObject();
        jsonShowChat.put("option", OPTION_SHOW_CHATROOMS);
        writeInBuffer(jsonShowChat.toJSONString(), bw);
    }

    /**
     * Join the client to the requested chatroom.
     *
     * @param jsonOptionChosen
     * @param client
     */
    public static void createChatroom(Client client, JSONObject jsonOptionChosen) {

        Scanner keyboard = new Scanner(System.in);

        System.out.println(System.lineSeparator() + "Write the name of the chatroom: ");
        String name = keyboard.nextLine();
        while (name.isEmpty()) {

            name = checkStringEmpty(System.lineSeparator() + "Write the name of the chatroom: ");
        }

        jsonOptionChosen.put("name", name);

        System.out.println(System.lineSeparator() + "Write the thematic of the chatroom");
        String thematic = keyboard.nextLine();
        while (thematic.isEmpty()) {

            thematic = checkStringEmpty(System.lineSeparator() + "Write the thematic of the chatroom: ");
        }

        jsonOptionChosen.put("thematic", thematic);

        // Tells the client (object class) that it is in chat mode.
        client.setIsInChat(true);
    }

    /**
     * Join the client to the requested chatroom.
     *
     * @param jsonOptionChosen
     * @param keyboard
     * @param bw
     * @param readerRunnable
     * @throws IOException
     * @return
     */
    public static boolean joinChatroom(JSONObject jsonOptionChosen, Scanner keyboard, BufferedWriter bw, ReaderRunnable readerRunnable) throws IOException {

        int positionChosen = 0;

        // Shows the current chatrooms. This will not display nothing if there are none.
        showChatrooms(bw);

        // This will make the reader runnable wait 1 second for the server response.
        readerRunnable.setWaitingToServer(true);
        synchronizeCurrentThread(readerRunnable);

        // If there are chatrooms in server, ask the user to enter the position of the desired chatroom.
        if (!readerRunnable.isEmptyChatrooms()) {

            System.out.println(System.lineSeparator() + "Type the number of the chatroom you want to join: "
                    + System.lineSeparator());

            keyboard.nextLine();
            while (!checkValidOption(keyboard)) {

                System.out.println(System.lineSeparator() + "The number of the chatroom must be a number, please, try again");
                keyboard.nextLine();
            }

            positionChosen = keyboard.nextInt();
            jsonOptionChosen.put("chatpositioninlist", positionChosen);

        }

        // Returns true/false if there are chatrooms or not.
        return !readerRunnable.isEmptyChatrooms();
    }

    /**
     * Perform an action depending if the client requests close the program, get
     * help o return to main menu.
     *
     * @param message
     * @param client
     * @param jsonOptionChosen
     * @param bw
     * @throws IOException
     */
    public static void executeOptions(String message, Client client, JSONObject jsonOptionChosen, BufferedWriter bw) throws IOException {

        if (message.equalsIgnoreCase(EXIT_KEY)) {

            jsonOptionChosen.put("option", OPTION_EXIT);
            writeInBuffer(jsonOptionChosen.toJSONString(), bw);
            client.getClientSocket().close();

        } else if (message.equalsIgnoreCase(HELP_KEY)) {

            jsonOptionChosen.put("option", OPTION_HELP);
            writeInBuffer(jsonOptionChosen.toJSONString(), bw);

        } else if (message.equalsIgnoreCase(BACK_KEY)) {

            jsonOptionChosen.put("option", OPTION_LEAVE_CHATROOM);
            writeInBuffer(jsonOptionChosen.toJSONString(), bw);

            // Sets the client out of the chat mode.
            client.setIsInChat(false);

        } else {

            jsonOptionChosen.put("messages", sendMessages(client, message));
        }
    }

    /**
     * Creates a new client instance and assigns to its nickname a random color.
     *
     * @param nickname
     * @return
     *
     */
    public static Client createNewClient(String nickname) {

        int posc = new Random().nextInt(COLOR_MESSAGES.size());
        String color = "";

        color = (String) COLOR_MESSAGES.keySet().toArray()[posc];

        return new Client(nickname, color, new Socket());
    }

    /**
     * Writes the string passed by parameter in the specified buffered writer.
     *
     * @param stringToWrite
     * @throws IOException
     * @param bw
     *
     */
    private static void writeInBuffer(String stringToWrite, BufferedWriter bw) throws IOException {

        bw.write(stringToWrite);
        bw.newLine();
        bw.flush();
    }

    public static void putColorsInHashmap(String key, String value) {

        COLOR_MESSAGES.put(key, value);
    }

    /**
     * Send the messages to the server.
     *
     * @param client
     * @param message
     * @throws IOException
     * @return
     */
    public static JSONObject sendMessages(Client client, String message) throws IOException {

        JSONObject jsonFather = new JSONObject();

        jsonFather.put("color", client.getColorKey());
        jsonFather.put("nickname", client.getNickname());
        jsonFather.put("message", " <[ " + message);
        jsonFather.put("hour", LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + " ]");
        return jsonFather;
    }

    /**
     * Creates new ReaderRunnable instance and starts reader threads for every
     * current client thread.
     *
     * @param currentClient
     * @return
     */
    public static ReaderRunnable readMessagesFromServer(Client currentClient) {

        ReaderRunnable readerRunnable = new ReaderRunnable(currentClient);
        Thread readerThread = new Thread(readerRunnable, "reader-" + currentClient.getNickname());
        readerThread.start();
        return readerRunnable;
    }

    public static void printMainMenu() {

        System.out.println(System.lineSeparator() + "Main menu" + System.lineSeparator());
        System.out.println(OPTION_CREATE_CHATROOM + ". Create chatroom");
        System.out.println(OPTION_JOIN_CHAT + ". Join chat rooms");
        System.out.println(OPTION_EXIT + ". Exit application");
        System.out.println(System.lineSeparator() + "Choose an option: ");
    }

    /**
     * Checks if the String passed by parameter is empty.
     *
     * @param message
     * @return
     */
    public static String checkStringEmpty(String message) {

        Scanner keyboard = new Scanner(System.in);
        System.out.println(System.lineSeparator() + message);

        return keyboard.nextLine();
    }

    /**
     * Checks if the option written is integer number
     *
     * @param keyboard
     * @return
     */
    public static boolean checkValidOption(Scanner keyboard) {

        return keyboard.hasNextInt();
    }
}