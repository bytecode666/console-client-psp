package chatclient;

import java.net.Socket;

public class Client {

    private String nickname;
    private String colorKey;
    private Socket clientSocket;
    private boolean isInChat;

    public Client(String nickname, String colorKey, Socket clientSocket) {

        this.clientSocket = clientSocket;
        this.colorKey = colorKey;
        this.nickname = null;
        this.isInChat = false;
        
        if (!nickname.isEmpty()) {
            this.nickname = nickname;
        }
    }

    /* This is synchronized because many threads will access to this resource.
    This is used to know if the client is chatting on a chatroom or not.*/
    public synchronized boolean getIsInChat() {
        return this.isInChat;
    }

    /* This is synchronized because many threads will access to this resource.
    This is used to assign when the client is chatting or not.*/
    public synchronized void setIsInChat(boolean isInChat) {
        this.isInChat = isInChat;
    }

    public String getNickname() {
        return this.nickname;
    }

    public Socket getClientSocket() {
        return this.clientSocket;
    }

    public String getColorKey() {
        return colorKey;
    }

    @Override
    public String toString() {
        return this.nickname;
    }
}